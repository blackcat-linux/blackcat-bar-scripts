#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

readonly default_out="/tmp/blackcat-bar/xbps.txt"

OUTFILE=${OUTFILE:-"$default_out"}

usage() {
    echo "USAGE: $0 [OPTIONS]"
    echo ""
    echo "OPTIONS:"
    echo "    -o <outfile>   outfile [default: $default_out]"
    echo "    -h             prints this message"
}

while getopts ':ho:' opt; do
    case "$opt" in
        'h')
            usage
            exit 0
            ;;
        'o')
            OUTFILE="$OPTARG"
            ;;
        '?')
            echo "unknown option $opt"
            usage
            exit 1
            ;;
        :)
            echo "other"
            ;;
    esac
done

if [ ! -d "$OUTFILE" ]; then
    mkdir "$(dirname "$OUTFILE")"
fi

main() {
    updates=$(xbps-install -Mun 2> /dev/null | wc -l)

    if [ -n "$updates" ] && [ "$updates" -ge 0 ]; then
        printf  "%s" "$updates" > "$OUTFILE"
    else
        printf  "%s" "ERROR: unable to get updates" > "$OUTFILE"
    fi
}

main

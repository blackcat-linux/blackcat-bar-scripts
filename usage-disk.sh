#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

readonly default_out="/tmp/blackcat-bar/usage-disk.txt"
readonly default_mount="/"

MOUNT=${MOUNT:-"$default_mount"}
OUTFILE=${OUTFILE:-"$default_out"}

usage() {
    echo "USAGE: $0 [OPTIONS]"
    echo ""
    echo "OPTIONS:"
    echo "    -o <outfile>   outfile [default: $default_out]"
    echo "    -m <mount>     mount point [default: $default_mount]"
    echo "    -h             prints this message"
}

while getopts ':ho:m:' opt; do
    case "$opt" in
        'h')
            usage
            exit 0
            ;;
        'o')
            OUTFILE="$OPTARG"
            ;;
        'm')
            MOUNT="$OPTARG"
            ;;
        '?')
            echo "unknown option $opt"
            usage
            exit 1
            ;;
        :)
            echo "other"
            ;;
    esac
done

if [ ! -d "$OUTFILE" ]; then
    mkdir "$(dirname "$OUTFILE")"
fi

main() {
    printf "%s" "$(df -h -P "$MOUNT" | tail -1 | awk '{print $3 "/" $4}')" > "$OUTFILE"
}

main
